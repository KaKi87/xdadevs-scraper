const
    axios = require('axios'),
    { Window } = require('happy-dom'),
    numbro = require('numbro'),
    getTimeElementTimestamp = element => new Date(element.getAttribute('datetime')).valueOf(),
    client = axios.create({ baseURL: 'https://forum.xda-developers.com' });

client.interceptors.response.use(response => {
    const window = new Window();
    window.document.documentElement.innerHTML = response.data;
    response.document = window.document;
    return response;
});

module.exports = {
    searchForum: async ({
        query
    }) => {
        const {
            document
        } = await client(
            'search/1',
            {
                params: {
                    'q': query,
                    't': 'node',
                    'c[title_only]': '1',
                    'o': 'relevance'
                }
            }
        );
        return document.querySelectorAll('.contentRow-title').map(item => {
            const itemLink = item.querySelector('a');
            return {
                id: itemLink.getAttribute('href').slice(1, -1),
                title: itemLink.textContent,
                label: item.querySelector('.label').textContent
            };
        });
    },
    getForums: async ({
        parentForumId
    }) => {
        const {
            document
        } = await client(
            parentForumId
        );
        return document.querySelectorAll('.node-main').map(item => {
            const
                itemLink = item.querySelector('a'),
                itemStats = item.querySelectorAll('dd');
            return {
                id: itemLink.getAttribute('href').slice(1, -1),
                title: itemLink.textContent,
                description: item.querySelector('.node-description').textContent,
                threadCount: parseInt(itemStats[0].textContent),
                commentCount: parseInt(itemStats[1].textContent)
            };
        });
    },
    getThreads: async ({
        forumId,
        pageNumber
    }) => {
        const
            {
                document
            } = await client(
                `${forumId}${pageNumber ? `/page-${pageNumber}` : ''}`
            ),
            result = document.querySelectorAll('.structItem--thread').map(item => {
                const
                    itemLink = item.querySelector('.structItem-title a'),
                    itemStats = item.querySelectorAll('dd'),
                    itemLastPostAuthor = item.querySelector('.structItem-cell--latest .username'),
                    creationTimestamp = getTimeElementTimestamp(item.querySelector('.structItem-startDate time')),
                    lastPostCreationTimestamp = getTimeElementTimestamp(item.querySelector('.structItem-latestDate'));
                return {
                    id: itemLink.getAttribute('href').slice(1, -1),
                    title: itemLink.textContent,
                    creationTimestamp,
                    authorId: item.querySelector('.structItem-cell--main .username').getAttribute('href').slice(1, -1),
                    authorUsername: item.getAttribute('data-author'),
                    isAuthorStaff: !!item.querySelector('.username--staff'),
                    isLocked: !!item.querySelector('.structItem-status--locked'),
                    isSticky: !!item.querySelector('.structItem-status--sticky'),
                    replyCount: numbro.unformat(itemStats[0].textContent.toLowerCase()),
                    viewCount: numbro.unformat(itemStats[1].textContent.toLowerCase()),
                    score: parseInt(item.querySelector('.structItem-cell--meta').getAttribute('title').split(' ').slice(-1)[0]),
                    ...creationTimestamp !== lastPostCreationTimestamp ? {
                        lastPostCreationTimestamp,
                        lastPostAuthorId: itemLastPostAuthor.getAttribute('href').slice(1, -1),
                        lastPostAuthorUsername: itemLastPostAuthor.textContent,
                        isLastPostAuthorStaff: !!itemLastPostAuthor.querySelector('.username--staff')
                    } : {}
                };
            });
        result.pageCount = parseInt(document.querySelector('.pageNav-page:last-child').textContent);
        return result;
    },
    getPosts: async ({
        threadId,
        pageNumber
    }) => {
        const
            {
                document
            } = await client(
                `${threadId}${pageNumber ? `/page-${pageNumber}` : ''}`
            ),
            result = document.querySelectorAll('.message').map(item => {
                const itemEditionTimestamp = item.querySelector('[itemprop="dateModified"]');
                return {
                    authorUsername: item.getAttribute('data-author'),
                    creationTimestamp: getTimeElementTimestamp(item.querySelector('[itemprop="datePublished"]')),
                    editionTimestamp: itemEditionTimestamp ? getTimeElementTimestamp(itemEditionTimestamp) : undefined,
                    content: item.querySelector('.message-body').textContent.trim()
                };
            });
        result.pageCount = parseInt(document.querySelector('.pageNav-page:last-child').textContent);
        return result;
    }
};